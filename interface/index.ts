//로그인

export interface UserPayloadInterface {
  username: string;
  password: string;
}

//문의글
//문의글 등록
export interface IAskRequest {
  title: string
  content: string
  multipartFile?: string;
}

//문의글 리스트
export interface IAskResponse {
  id: number
  title: string
  content: string
  isAnswer: boolean
  dateMemberAsk: string
}
// 문의글 상세보기
export interface IAskDetailResponse {
  memberName: string
  content: string
  isAnswer: boolean
  dateMemberAsk: string
}
// 문의글 답변 보기
export interface IAskComment {
  answerTitle: string
  answerContent: string
  answerImgUrl: string
  dateAnswer: string
}


//사업장
//사업장 수정
export interface IBusinessEdit {
  businessName: string
  ownerName: string
  businessImgUrl: string
  businessType: string
  businessLocation: string
  businessEmail: string
  businessPhoneNumber: string
  reallyLocation: string
}
//내 사업장 조회
export interface IBusiness {
  businessName: string
  businessNumber: string
  ownerName: string
  businessImgUrl: string
  businessType: string
  businessLocation: string
  businessEmail: string
  businessPhoneNumber: string
  dateApprovalBusiness: string
  dateJoinBusiness: string
  reallyLocation: string
  ectMemo: string
  isApprovalBusiness: boolean
  isActivity: boolean
}

//사업장 멤버
export interface IBusinessMember {
  memberName: boolean
  businessName: string
  position: string
  dateIn: string
  weekSchedule: string[]
  etc: string
}

//사업장 멤버 등록
export interface IBusinessMemberAdd {
  phoneNumber: string
  position: string
  etc: string
}

//사업장 멤버 스케쥴 정보 수정
export interface IBusinessMemberPut {
    weekSchedule: string
    timeScheduleStart: string
    timeScheduleEnd: string
    timeScheduleTotal: string
}

export interface IBusinessTableColumn {
  label: string
  field: string
  width: string
  sortable: boolean
}


//공지사항 리스트
export interface INoticeResponse {
  id: number
  memberType: string
  title: string
  dateNotice: string
}

//공지사항 상세보기
export interface INoticeDetailResponse {
  id: number
  memberType: string
  title: string
  content: string
  dateNotice: string
  noticeImgUrl: string
}

//재고 관리 리스트
export interface IProductRequest {
  productName: string
  minQuantity: number
  nowQuantity: number
}

//재고 등록
export interface IProductResponse {
  id: string
  memberName: string
  dateProduct: string
  productName: string
  minQuantity: number
  nowQuantity: number
}

//재고 수정 (수정해야함)
export interface IProductEdit {
  productName: string
  minQuantity: number
}

export interface IProduct {
  nowQuantity: number
}


//인수인계 등록
export interface ITransition {
  content: string
}

//근로계약 등록
export interface IContractRequest {
  dateContract: string,
  payType: string,
  payPrice: number,
  dateWorkStart: string,
  dateWorkEnd: string,
  restTime: number,
  insurance: string,
  workDay: string,
  workTime: number,
  weekWorkTime: number,
  isWeekPay: boolean,
  mealPay: number,
  wagePayment: string,
  wageAccountNumber: string,
  contractCopyImgUrl: string
}
