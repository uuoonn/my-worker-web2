import type {IAskComment, IAskDetailResponse, IAskRequest, IAskResponse} from "~/interface";

export const useAskStore = defineStore('askAnswer', {
  state: () => ({
    list: [] as IAskResponse[],
    listDetail: [] as IAskDetailResponse[],
    comment: [] as IAskComment[],
    totalPage: 0,
  }),
  actions: {
    // 문의글 등록
    async setAsk(data: IAskRequest) {
      const token = useCookie('token');

      // 전달된 데이터 디스트럭처링
      const { title, content, multipartFile } = data;

      // 폼 데이터 생성
      const formData = new FormData();

      // title과 content 추가
      formData.append('title', title);
      formData.append('content', content);

      // multipartFile이 있는 경우에만 추가
      if (multipartFile) {
        formData.append('multipartFile', multipartFile);
      }

      try {
        await $fetch('http://versuss.store:8080/v1/member-ask/new', {
          method: 'POST',
          body: formData, // 폼 데이터 전송
          headers: {
            'Authorization': `Bearer ${token.value}`,
            //'Content-Type': 'multipart/form-data'
          },
        });

        // 응답 처리 (필요하다면)
      } catch (error) {
        console.error("문의글 등록 실패:", error);
        throw error;
      }
    },


    // 내가 쓴 문의글 리스트
    //페이징 처리 완료
    async fetchAsk(pageNum: number) {
      const token = useCookie('token');
      const { data }:any = await useFetch(`http://versuss.store:8080/v1/member-ask/all/member/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        },
      })
      if (data) {
        this.list = data.value.list;
        this.totalPage = data.value.totalPage;
      }
      console.log(this.list)
      console.log(this.totalPage)
    },

    //내가 쓴 문의글 상세보기
    async getDetail(memberAskId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params;
      const { data }:any = await useFetch(`http://versuss.store:8080/v1/member-ask/detail/member-ask-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
      if (data) {
        //내가 쓴 문의글
        this.listDetail = data.value.data;
        //문의글에 달린 답변
        this.comment = data.value.data.comment;
      }
    },

    //문의글 삭제
    async delAsk(memberAskId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params;
      const { data }:any = await useFetch(`http://versuss.store:8080/v1/member-ask/del/member-ask-id/${id}`, {
        method: 'DELETE',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
      if (data) {
        this.listDetail = data.value.data;
      }
    },
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useAskStore, import.meta.hot))
}
