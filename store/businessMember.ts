import { defineStore } from 'pinia'
import type {IAskRequest, IBusinessMemberAdd, IBusinessMemberPut} from "~/interface";

// 사업장 멤버 스토어
export const useBusinessMemberStore = defineStore('businessMember', {
  state: () => ({
    MemberList: [],
    MemberDetail: [],
    MemberWeek: [],
    MyInfo: [],
    weekSchedule: [],
    timeScheduleStart: [],
    timeScheduleEnd: [],
    timeScheduleTotal: [],
    memberWeek: '',
    memberWeekArray: {},
    todaySchedule: [],
    totalPage: 0,
  }),

  actions: {
    //내정보
    async getMyInfo() {
      const token = useCookie('token');
      const {data}: any = await useFetch(`http://versuss.store:8080/v1/member/detail`, {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
      if (data) {
        this.MyInfo = data.value.data;
        console.log(this.MyInfo)
      }
    },

    //직원 등록
    async setAddMember(data: IBusinessMemberAdd) {
      const token = useCookie('token');
      await $fetch(`http://versuss.store:8080/v1/business-member/join`, {
        method: 'POST',
        body: data,
        headers: {
          'Authorization' : `Bearer ${token.value}`,
        }
      })
    },
    //직원 근무일정 등록 및 수정
    async putOut(businessMemberId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params;
      await $fetch(`http://versuss.store:8080/v1/business-member/change/work/business-member-id/${id}`, {
        method: 'PUT',
        headers: {
          'Authorization' : `Bearer ${token.value}`,
        }
      })
    },

    //직원 근무일정 등록 및 수정
    async putSchedule(data: IBusinessMemberPut,businessMemberId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params;
      await $fetch(`http://versuss.store:8080/v1/business-member/change/schedule/business-member-id/${id}`, {
        method: 'PUT',
        body: data,
        headers: {
          'Authorization' : `Bearer ${token.value}`,
        }
      })
    },

    //내 직원 조회
    async getMember(pageNum: number) {
      const token = useCookie('token');
      const {data}: any = await useFetch(`http://versuss.store:8080/v1/business-member/all/my-business/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
      if (data) {
        this.MemberList = data.value.list;
        this.totalPage = data.value.totalPage;
      }
    },

    // 직원 상세 조회
    async getDetail(businessMemberId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params
      const {data}: any = await useFetch(`http://versuss.store:8080/v1/business-member/detail/business-member-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
      if (data) {
        this.MemberDetail = data.value.data;
        this.weekSchedule = data.value.data.weekSchedule;
        this.timeScheduleStart = data.value.data.timeScheduleStart;
        this.timeScheduleEnd = data.value.data.timeScheduleEnd;
        this.timeScheduleTotal = data.value.data.timeScheduleTotal;
        this.MemberWeek = data.value.data.weekScheduleList;
      }
      return {
        MemberDetail: this.MemberDetail,
        weekSchedule:this.weekSchedule,
        timeScheduleStart:this.timeScheduleStart,
        timeScheduleEnd:this.timeScheduleEnd,
        timeScheduleTotal:this.timeScheduleTotal,
        MemberWeek: this.MemberWeek };
    },

    //날짜 가져와서 선택 날짜 출퇴근자 보기
    //날짜 가져와서 오늘날짜 출퇴근자 보기
    async getTodaySchedule(today) {
      const token = useCookie('token');

      try {
        const date = new Date(today);
        const nextDay = new Date(date.setDate(date.getDate() + 1)); // 선택한 날짜에 1일을 더함 왜??? 서버시간이랑 한국시간이랑 달라서..ㅜㅜ
        const formatDate = nextDay.toISOString().split('T')[0]; // 근디 이렇게 하면..
        //const formatDate = date.toISOString().split('T')[0]; // 이 경우는 서버 날짜 = 한국날짜 인 경우 아닌 경우엔 위에 값 두줄로..
        const response = await useFetch(`http://versuss.store:8080/v1/schedule/calendar/day?today=${formatDate}`, {
          method: 'GET',
          headers: {
            'Authorization' : `Bearer ${token.value}`
          },
        });

        this.todaySchedule = response.data.value.list;

      } catch (error) {
        console.error("오늘 출퇴근자 로드 실패:", error);
        throw error;
      }
    },
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useBusinessMemberStore, import.meta.hot))
}
