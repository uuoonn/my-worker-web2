// 사이드바 메뉴
export default [
  // 대시보드메뉴
  {
    isTitle: false,
    name: "대시보드",
    url: "/",
    icon: "home",
    submenu: [],
  },
  {
    isTitle: false,
    name: "직원 관리",
    url: "/business/member/table",
    icon: "users",
    submenu: [],
  },
  {
    isTitle: false,
    name: "출퇴근 리스트",
    url: "/business/schedule/today-list",
    icon: "pie-chart",
    submenu: [],
  },
  {
    isTitle: false,
    name: "근로 계약 관리",
    url: "/contract",
    icon: "file-text",
    submenu: [],
  },
  //상품관리
  {
    isTitle: false,
    name: "메뉴얼 관리",
    url: "/manual",
    icon: "file",
    submenu: [],
  },
  {
    isTitle: false,
    name: "상품 재고 관리",
    url: "/product/product-table",
    icon: "archive",
    submenu: [],
  },
  {
    isTitle: false,
    name: "인수인계 캘린더",
    url: "/transition/calendar",
    icon: "calendar",
    submenu: [],
  },
  //마이페이지
  {
    isTitle: false,
    name: "마이페이지",
    icon: "align-justify",
    submenu: [
      {
        isTitle: false,
        name: "내 정보",
        url: "/profile",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "공지사항",
        url: "/notice",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "1:1 문의",
        url: "/ask/member-ask-list",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },
  // {
  //   isTitle: false,
  //   name: "X근무 일정 캘린더",
  //   url: "/business/schedule/calendar",
  //   icon: "calendar",
  //   submenu: [],
  // },
  // {
  //   isTitle: false,
  //   name: "X급여 관리",
  //   url: "",
  //   icon: "dollar-sign",
  //   submenu: [
  //     {
  //       isTitle: false,
  //       name: "급여 등록",
  //       url: "/register",
  //       icon: "chevron-right",
  //       submenu: [],
  //     },
  //     {
  //       isTitle: false,
  //       name: "급여 조회",
  //       url: "/users",
  //       icon: "chevron-right",
  //       submenu: [],
  //     },
  //   ],
  // },

];
