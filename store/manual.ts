import type {IAskRequest} from "~/interface";

export const useManualStore = defineStore('manual', {
  state: () => ({
    list: [],
    detail: [],
    totalPage: 0,
  }),
  actions: {
    async setManual(data: IAskRequest) {
      const token = useCookie('token');

      // 전달된 데이터 디스트럭처링
      const { title, content, multipartFile } = data;

      // 폼 데이터 생성
      const formData = new FormData();

      // title과 content 추가
      formData.append('title', title);
      formData.append('content', content);

      // multipartFile이 있는 경우에만 추가
      if (multipartFile) {
        formData.append('multipartFile', multipartFile);
      }

      try {
        await $fetch('http://versuss.store:8080/v1/manual/new/', {
          method: 'POST',
          body: formData, // 폼 데이터 전송
          headers: {
            'Authorization': `Bearer ${token.value}`,
          },
        });

        // 응답 처리 (필요하다면)
      } catch (error) {
        console.error("메뉴얼 등록 실패:", error);
        throw error;
      }
    },
    // 메뉴얼 리스트
    async getManuals(pageNum: number) {
      const token = useCookie('token');
      const { data }:any = await useFetch(`http://versuss.store:8080/v1/manual/all/boss/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
      if (data) {
        this.list = data.value.list;
        this.totalPage = data.value.totalPage;
      }
    },

    //메뉴얼 상세보기
    async getDetail(noticeId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params;
      const { data }:any = await useFetch(`http://versuss.store:8080/v1/manual/detail/manual-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
      if (data) {
        this.detail = data.value.data;
      }
    },

  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useManualStore, import.meta.hot))
}
