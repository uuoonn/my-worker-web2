import type {IProduct, IProductEdit, IProductRequest, IProductResponse} from "~/interface";

export const useProductStore = defineStore('product', {
  state: () => ({
    list: [] as IProductResponse[],
    detail: [],
    totalPage: 0,

  }),
  actions: {
    // 상품 등록
    async setProduct(data: IProductRequest) {
      const token = useCookie('token');
      await $fetch(`http://versuss.store:8080/v1/product/new/boss`, {
        method: 'POST',
        body: data,
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
    },

// 상품 재고 리스트
    async getProducts(pageNum: number) {
      const token = useCookie('token');
      const { data }: any = await useFetch(`http://versuss.store:8080/v1/product/all/boss/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });

      if (data) {
        this.list = data.value.list;
        this.totalPage = data.value.totalPage;

        // 각 항목을 반복하여 필요한 정보를 추출하고 반환
        const extractedData = this.list.map((item: any) => {
          return {
            id: item.id,
            productName: item.productName,
            minQuantity: item.minQuantity,
            nowQuantity: item.nowQuantity
          };
        });
        return extractedData;
      }

      return []; // 데이터가 없을 경우 빈 배열 반환
    },



    // 상품 재고 상세보기
    async getDetail(productId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params;
      const { data }:any = await useFetch(`http://versuss.store:8080/v1/product/detail/product-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        },
      })
      if (data) {
        this.detail = data.value.data;
      }
    },

    // 상품 재고 수량 수정 (최소수량)
    async putProduct(data: IProductEdit, productId: string) {
      const token = useCookie('token');
      await $fetch(`http://versuss.store:8080/v1/product/change/product-id/${productId}`, {
        method: 'PUT',
        body: data,
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
    },

    // 상품 재고 수량 수정 (현재 수량)
    async setProductEdit(data: IProduct, productId: string) {
      const token = useCookie('token');
      await $fetch(`http://versuss.store:8080/v1/product-record/boss/product-id/${productId}`, {
        method: 'POST',
        body: data,
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
    },

    // 상품 삭제
    async delProduct(productId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params;
      const { data }:any = await $fetch(`http://versuss.store:8080/v1/product/product-id/${productId}`, {
        method: 'DELETE',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
      if (data) {
        this.detail = data.value.data;
      }
    },
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useProductStore, import.meta.hot))
}
