import { defineStore } from 'pinia'
import type {IBusiness, IBusinessEdit} from "~/interface";

//사업장 정보 스토어
export const useBusinessStore = defineStore('business', {
  state: () => ({
    businessInfo: [] as any,
  }),

  actions: {
    //내 사업장 조회
    async getBusinessInfo() {
      const token = useCookie('token');
        const {data}: any = await useFetch('http://versuss.store:8080/v1/business/detail/my/business', {
          method: 'GET',
          headers: {
            'Authorization' : `Bearer ${token.value}`
          }
        })
      if (data) {
        this.businessInfo = data.value.data;
      }
    },

    async setCreateRe(data: IBusinessEdit ,businessId: string) {
      const token = useCookie('token');
      // const { id }: any = this.businessId;
      // businessId만 잘 받아올수 있으면 됨
      await $fetch(`http://versuss.store:8080/v1/request-fix/join/business-id/1`, {
        method: 'POST',
        body: data,
        headers: {
          'Authorization' : `Bearer ${token.value}`,
        }
      })
    }
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useBusinessStore, import.meta.hot))
}
