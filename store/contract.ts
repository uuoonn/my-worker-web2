import type {IContractRequest} from "~/interface";

export const useContractStore = defineStore('contract', {
  state: () => ({
    list: [],
    detail: [],
    totalPage: 0,

  }),
  actions: {
    // 계약 등록
    async setContract(data: IContractRequest) {
      const token = useCookie('token');
      await $fetch(`http://versuss.store:8080/v1/contract/join/business-memberId/1`, {
        method: 'POST',
        body: data,
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
    },

    // 근로 계약 리스트
    async getContracts(pageNum: number) {
      const token = useCookie('token');
      const { data }: any = await useFetch(`http://versuss.store:8080/v1/contract/all/boss/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      });

      if (data) {
        this.list = data.value.list;
        this.totalPage = data.value.totalPage;
      }
    },

    // 근로 계약 상세보기
    async getDetail(contractId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params;
      const { data }:any = await useFetch(`http://versuss.store:8080/v1/contract/detail/contract-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        },
      })
      if (data) {
        this.detail = data.value.data;
      }
    },

  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useContractStore, import.meta.hot))
}
