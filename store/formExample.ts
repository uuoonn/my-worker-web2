// 입력폼 예시

import { defineStore } from "pinia";
import { useDialog, useToast } from "vue3-tailwind";
import { useForm } from "vue3-tailwind";

const toast = useToast();
const dialog = useDialog();
const composableForm = useForm();

export const useFormExample = defineStore("formExample", () => {
  const formName = "formExample";

  const formData: {
    [key: string]: any;
  } = reactive({
    fileModel: null,
    selectExample: null,
    multiSelectExample: null,
    inputExample: null,
    inputExample1: null,
    inputExample2: null,
    inputExample3: null,
    inputExample4: null,
    inputExample5: null,
    inputExample6: null,
    toggleExample: null,
  });

  //글자수 제한 문구
  const formRules = {
    inputExample: ["required", "string"],
    inputExample1: ["required", "string"],
    inputExample2: ["required", "string"],
    inputExample3: ["required", "string"],
    inputExample4: ["required", "string"],
    inputExample5: ["required", "string"],
    inputExample6: ["required", "string"],
    textAreaExample: [
      "required",
      // "string",
      "test",
      (value: string) => {
        const MIN_LENGTH = 12;
        if (!value || value.length < MIN_LENGTH) {
          return `최소 글자수는 ${MIN_LENGTH}이고, 현재 글자수는 ${value.length}입니다.`;
        }
      },
    ],

    selectExample: ["required"],

    // 토글 문구
    toggleExample: [
      "required",
      // "boolean",
      (value: string) => {
        if (!value) return "필수 약관에 동의해 주세요.";
      },
    ],
  };

  const isError = ref(false);

  const form = computed(() => composableForm.getForm(formName));
  const validator = computed(() => form.value.validator);

  //제출용 버튼 안내 문구
  async function submit() {
    const isConfirmed = await dialog.fire({
      title: "제출하시겠습니까?",
      description: "제출시 수정할 수 없습니다.한번 더 확인해주세요!",
    });
    if (!isConfirmed) return;
    validator.value.clearErrors();
    await validator.value.validate();
    if (validator.value.fail()) {
      toast.error({
        message: validator.value.getErrorMessage(),
      });
      isError.value = true;
      setTimeout(() => {
        isError.value = false;
      }, 1000);
    }
  }

  function clear() {
    formData.fileModel = null;
    formData.selectExample = null;
    formData.multiSelectExample = null;
    formData.inputExample = null;
    formData.inputExample1 = null;
    formData.inputExample2 = null;
    formData.inputExample3 = null;
    formData.inputExample4 = null;
    formData.inputExample5 = null;
    formData.inputExample6 = null;
    formData.textAreaExample = null;
    formData.toggleExample = null;

    validator.value.clearErrors();
  }

  const selectionList = [
    {
      label: "test",
      value: "test",
    },
    {
      label: "test2",
      value: "test2",
    },
    {
      label: "test3",
      value: "test3",
    },
  ];

  return {
    formName,
    formData,
    formRules,
    isError,
    selectionList,
    submit,
    clear,
  };
});
