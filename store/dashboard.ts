import { defineStore } from 'pinia'

//대시보드
export const useDashboard = defineStore('dashboard', {
  state: () => ({
    today: [],
    todayStart: [],
    todayLate: [],
    transition: [],
    notice: [],
    product: []
  }),

  actions: {
    async getTodayCheck() {
      const token = useCookie('token');
      const {data}: any = await useFetch('http://versuss.store:8080/v1/chart/schedule/chart', {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
      if (data) {
        this.today = data.value.data.businessMemberStartDayItem;
        this.todayStart = data.value.data.scheduleLatenessNames;
        this.todayLate = data.value.data.scheduleOutLatenessNames;
      }
    },
    async getDash() {
      const token = useCookie('token');
      const {data}: any = await useFetch('http://versuss.store:8080/v1/dashboard/full-view/', {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
      if (data) {
        this.transition = data.value.data.transitionDashBoardItems;
        this.notice = data.value.data.noticeDashBoardItems;
        this.product = data.value.data.productDashBoardItems;
      }
    },
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useDashboard, import.meta.hot))
}
