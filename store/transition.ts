import type {
  ITransition
} from "~/interface";
import {use} from "h3";
import type { months } from "moment";

export const useTransitionStore = defineStore('transition', {
  state: () => ({
    weekList: [],
    detail: [],

  }),
  actions: {

    async setTransition(data: ITransition) {
      const token = useCookie('token');
      await $fetch(`http://versuss.store:8080/v1/transition/new/boss`, {
        method: 'POST',
        body: data,
        headers: {
          'Authorization': `Bearer ${token.value}`
        }
      })
    },

    //
    async putTransition(transitionId:string) {
      const token = useCookie('token');
      await $fetch(`http://versuss.store:8080/v1/transition/finish/transition-id/${transitionId}`, {
        method: 'PUT',
        headers: {
          'Authorization': `Bearer ${token.value}`
        }
      })
    },
    // 한달용 인수인계 리스트
    async getTodoMonth(): Promise<void> {
      // 오늘 날짜 구하기
      const today = new Date;
      //오늘 날짜에서 연도 4자리 가져오기 + string 처리
      const year = today.getFullYear().toString();
      //getMonth로 월 정보 가져오기, 월은 0부터 시작하므로 1더해주기
      //문자열로 반환 후 , 문자열 길이가 2보다 작으면 문자열 앞에 0을 추가
      const month = (today.getMonth() + 1).toString().padStart(2,'0');
      const token = useCookie('token');
      const {data}: any = await useFetch(`http://versuss.store:8080/v1/transition/calendar?year=${year}&month=${month}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      })
      if (data) {
        this.weekList = data.value.list;
      }
    },
//모든 달 데이터 보여주기
    // async getAllTodoMonths(): Promise<void> {
    //   const today = new Date();
    //   const currentYear = today.getFullYear();
    //   const currentMonth = today.getMonth() + 1;
    //   const START_YEAR = 2024;
    //
    //   // 현재 월부터 1월까지 모든 달에 대한 데이터를 가져옴
    //   for (let year = currentYear; year >= START_YEAR; year--) {
    //     const endMonth = (year === currentYear) ? currentMonth : 12;
    //     const startMonth = (year === START_YEAR) ? 1 : 12;
    //     for (let month = endMonth; month >= startMonth; month--) {
    //       await this.getTodoMonth(year,month);
    //     }
    //   }
    // },

    // // 하루용 인수인계 리스트
    async getDetail() {
      const token = useCookie('token');
      const today = new Date;
      const year = today.getFullYear().toString();
      const month = (today.getMonth() + 1).toString().padStart(2,'0');
      const day = today.getDate().toString().padStart(2,'0');
      const yyyy_mm_dd = `${year}-${month}-${day}`;
      const {data}: any = await $fetch(`http://versuss.store:808/v1/transition/calender/day?localDate=${yyyy_mm_dd}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token.value}`
        },
      })
      if (data) {
        this.detail = data.value.data;
      }
    },
    //인수인계 삭제
    async delTransition(transitionId: string) {
      const token = useCookie('token');
      const {data}: any = await useFetch(`http://versuss.store:8080/v1/transition/transition-id/${transitionId}`, {
        method: 'DELETE',
        headers: {
          'Authorization': `Bearer ${token.value}`
        }
      })
      if (data.value) {
        this.weekList = data.value;
      }
      console.log(this.weekList)
    },
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useTransitionStore, import.meta.hot))
}
