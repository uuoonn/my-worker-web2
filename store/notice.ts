import type {INoticeDetailResponse, INoticeResponse} from "~/interface";

export const useNoticeStore = defineStore('notice', {
  state: () => ({
    list: [] as INoticeResponse[],
    detail: [] as INoticeDetailResponse[],
    totalPage: 0,
  }),
  actions: {

    // 공지사항 리스트
    async getNotice(pageNum: number) {
      const token = useCookie('token');
      const { data }:any = await useFetch(`http://versuss.store:8080/v1/notice/all/${pageNum}`, {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
      if (data) {
        this.list = data.value.list;
        this.totalPage = data.value.totalPage;
      }
    },

    //공지사항 상세보기
    async getNoticeDetail(noticeId: string) {
      const token = useCookie('token');
      const { id } = useRoute().params;
      const { data }:any = await useFetch(`http://versuss.store:8080/v1/notice/detail/notice-id/${id}`, {
        method: 'GET',
        headers: {
          'Authorization' : `Bearer ${token.value}`
        }
      })
      if (data) {
        this.detail = data.value.data;
      }
    },

  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useNoticeStore, import.meta.hot))
}
