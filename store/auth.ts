import { defineStore } from 'pinia';
import type {UserPayloadInterface} from "~/interface";

export const useAuthStore = defineStore('auth', {
  state: () => ({
    authenticated: false,
    loading: false,
  }),
  actions: {
    async authenticateUser({ username, password }: UserPayloadInterface) {
      const { data }: any = await useFetch('http://versuss.store:8080/v1/login/web/boss', {
        method: 'post',
        headers: { 'Content-Type': 'application/json' },
        body: {
          username,
          password,
        },
      });
      console.log(data)

      if (data.value) {
        const token = useCookie('token');
        token.value = data?.value?.data?.token;
        this.authenticated = true;
      }
    },
    logUserOut() {
      const token = useCookie('token');
      this.authenticated = false;
      token.value = null;
    },
  },
});
