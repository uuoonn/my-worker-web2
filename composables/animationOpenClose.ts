// 사이드바 애니메이션 효과
export const useAnimationOpenClose = () => {

  //엘리먼트 열 때
  const animateEnter = (el: HTMLElement) => {
    el.style.height = 'auto'
    const height = getComputedStyle(el).height

    el.style.height = '0'
    //딜레이 주는 효과
    setTimeout(() => {
      el.style.height = height;
    })
  }

  // 엘리먼트 열린 후 추가 애니메이션 효과
  const animateAfterEnter = (el: HTMLElement) => {
    el.style.height = 'auto'
  }

  //엘리먼트를 닫을 때 사용
  const animateLeave = (el: HTMLElement) => {
    el.style.height = getComputedStyle(el).height
    setTimeout(() => {
      el.style.height = '0';
    })
  }


  return {
    animateEnter,
    animateAfterEnter,
    animateLeave,
  }
}
