/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
    "./app.vue",
    "./node_modules/flowbite/**/*.js"
  ],
  darkMode: "class", // or 'media' or 'class'
  theme: {
    extend: {
      colors : {
        'main-color-1' : '#112D4E',
        'main-color-2' : '#3F72AF',
        'main-color-3' : '#DBE2EF',
        'main-color-4' : '#F9F7F7',
        'danger' : '#c81e1e',
      }
    },
  },
  plugins: [
    require("@tailwindcss/typography"),
    require('flowbite/plugin')
  ],
}
