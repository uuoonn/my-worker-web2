# 내 알바야 프로젝트
![image](./public/images/projectTop.jpg)

## 내 알바야 프로젝트 소개
* **아르바이트와 소규모 사업장의 효율적인 연결을 지원하는 통합 근무 관리 플랫폼**
* 사장님(사업장)이 아르바이트와 업무를 총괄하는 플랫폼 제작
* **개발기간** : 24.03.06~24.05.07
* **개발자** : 이윤진
* **기술 스택** : Vue3 / Nuxt.js / Swagger / Tailwind CSS / Pinia 
***
### 프로젝트 아키텍쳐
![image](./public/images/IMG_7172.png)
***
## 주요 기술
### 사용자 인증 관리
* 로그인 (Authenticate User): 사용자가 입력한 username과 password를 서버로 전송하여 인증을 수행합니다. 인증에 성공하면 서버로부터 받은 JWT 토큰을 클라이언트의 쿠키에 저장합니다.
* 로그아웃 (Log User Out): 쿠키에서 저장된 토큰을 삭제하고, 인증 상태를 해제합니다.
##### 사용 기술
* **Pinia**: Vue.js의 전역 상태 관리 라이브러리로, 상태(authenticated, loading)와 액션(authenticateUser, logUserOut)을 관리합니다.
* **TypeScript** 타입 안전성을 제공하여, 인증 로직에서 사용할 데이터 구조를 명확히 정의합니다.
* **Nuxt.js** useFetch: 서버에 비동기적으로 로그인 요청을 보내고, 응답 데이터를 받아 처리합니다.
* **쿠키 관리 (useCookie)**: JWT 토큰을 클라이언트 쿠키에 저장하고, 인증 상태를 관리합니다.
### 이미지 첨부 기능
* **사용자가 1:1 문의글을 작성할 때 이미지를 첨부할 수 있는 기능을 제공합니다.**
#### 구현 방법
* **폼데이터 구성** : 사용자가 이미지를 업로드하면, 해당 파일은 FormData 객체에 추가되어 HTML 폼 데이터를 쉽게 구성하고, 파일과 다른 텍스트 데이터를 함께 전송할 수 있도록 돕습니다. 문의글의 제목, 내용과 함께 이미지 파일이 multipart/form-data 형식으로 서버에 전송됩니다.
* **파일 수 제한 및 파일 변경 관리** : 파일 업로드 시 최대 1개의 파일만 첨부할 수 있도록 제한이 설정되어있습니다. 파일 수 제한을 초과할 경우 기존 파일을 제거하고 새로운 파일만 업로드하도록 처리합니다.

***
## 주요 기능
### GPS 기반 출퇴근 조회
* 아르바이트 또는 직원이 앱의 GPS를 이용해 출퇴근이 기록되면 사용자(사업자)가 선택한 날짜에 대한 직원의 출퇴근 기록을 확인할 수 있는 기능을 제공합니다.
* 직원 근태 정보를 한눈에 보기 쉽게 구현하였습니다. (정상 출근, 지각, 퇴근, 미출근)
### 직원 등록
* 직원 휴대폰 번호를 통해 데이터베이스에서 확인 후 사업장 내 직원으로 등록합니다.
* 등록된 직원만 내 사업장의 글을 볼 수 있습니다.
### 인수인계 캘린더
* 사업장내 직원들이 보다 효율적으로 한눈에 업무를 관리하기 위해 제공된 캘린더입니다.
* 일자별 인수인계 업무를 등록, 처리완료, 삭제할 수 있습니다.
### 상품 재고 관리
* 사업장에 등록된 모든 직원이 상품 재고를 관리하고 상품의 추가, 수정, 삭제 기능을 제공합니다.
* 최소수량보다 적은 수량을 보유한 경우, 대시보드에 해당 상품이 노출됩니다.
### 메뉴얼 관리
* 신규 입사 직원을 위한 사업장 메뉴얼을 등록, 수정, 삭제 기능을 제공합니다.

***
### 내 알바야 작업 영상 결과물
* [ 내 알바야 사장님 ] 웹/앱/백 구현 영상 [바로가기](https://www.youtube.com/watch?v=TT2fAmvc3mc&list=PLrYWWDy9kVB5NPXO2-bldKqagce7EXyBB&index=4)
* [ 내 알바야 알바생 / 관리자 ] 앱/웹 구현 영상 [바로가기](https://www.youtube.com/watch?v=-GLuWnb4N2M&list=PLrYWWDy9kVB5NPXO2-bldKqagce7EXyBB&index=4)
***
### 내 알바야 사장님 웹 구현 이미지
![image](./public/images/page1.png)
![image](./public/images/page2.png)
![image](./public/images/page3.png)
![image](./public/images/page4.png)
![image](./public/images/page5.png)
![image](./public/images/page6.png)
![image](./public/images/page7.png)
![image](./public/images/page8.png)
![image](./public/images/page9.png)
***
